// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPownBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPownBase::APlayerPownBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PownCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PownCamera"));
	RootComponent = PownCamera;

}

// Called when the game starts or when spawned
void APlayerPownBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPownBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPownBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPownBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPownBase::HandlePlayerHorizontalInput);

}

void APlayerPownBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPownBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMooveDirection!=EMoovementDirection::DOWN)
		{
			SnakeActor->LastMooveDirection = EMoovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMooveDirection != EMoovementDirection::UP)
		{
			SnakeActor->LastMooveDirection = EMoovementDirection::DOWN;
		}
	}
}

void APlayerPownBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value < 0 && SnakeActor->LastMooveDirection != EMoovementDirection::RIGHT)
		{
			SnakeActor->LastMooveDirection = EMoovementDirection::LEFT;
		}
		else if (value > 0 && SnakeActor->LastMooveDirection != EMoovementDirection::LEFT)
		{
			SnakeActor->LastMooveDirection = EMoovementDirection::RIGHT;
		}
	}
}



