// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MoovementSpeed = 10.f;
	LastMooveDirection = EMoovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MoovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Moove();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocatoin(SnakeElements.Num()*ElementSize, 0, 0);
		FTransform NewTransform(NewLocatoin);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}

	}
}

void ASnakeBase::Moove()
{
	FVector MoovementVector(ForceInitToZero);

	MoovementSpeed = ElementSize;
	
	switch (LastMooveDirection)
	{
	case EMoovementDirection::UP:
		MoovementVector.X += MoovementSpeed;
		break;
	case EMoovementDirection::DOWN:
		MoovementVector.X -= MoovementSpeed;
		break;
	case EMoovementDirection::LEFT:
		MoovementVector.Y += MoovementSpeed;
		break;
	case EMoovementDirection::RIGHT:
		MoovementVector.Y -= MoovementSpeed;
		break;
	}
	
//	AddActorWorldOffset(MoovementVector);

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

	}

	SnakeElements[0]->AddActorWorldOffset(MoovementVector);
}

